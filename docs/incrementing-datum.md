# How to use `auth-from-set` and `incrementing-datum` contracts

## What else I want to learn:
- how to compile a datum into a contract address??
- Are there methods for adding new datum to outgoing tx?

## Overview of Steps
1. Compile the contract found in `/at-5e40945`
2. Mint an AuthNFT (use bad Python)
3. Lock the AuthNFT at script Address (with substeps, use bad Python)
4. If testing `incrementing-datum`: Increment the Datum
5. Try to add and unlock funds from the script
6. Mint an NFT with the appropriate name, as passed from Redeemer
7. Add guards that only allow certain pkh to use tx
