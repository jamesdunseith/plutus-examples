#! python3
# mintingUtils.py

import subprocess, os
import pyinputplus as pyin

# choose collateral

# choose tx to consume

# Mint an NFT with a pre-compiled Plutus Script
# In this case, the script has no expiration date, and can mint without limit
# However, it can only mint 1 "AuthNFT" at a time. Different quantities or different names will not work

# needs refactoring and re-usability
def nftMinter(tx_dir, current):
    sender = 'address'
    senderkey = 'path to payment.skey'
    txin = 'utxo'
    collateral = 'utxo'
    policyID = '6b4c0ea94391ce0b909e06ee7aa3350ccb1aa2032928466d2959c3f2'
    tokenName = 'AuthNFT'
    scriptFile = 'path to mint-nft.plutus'
    lovelace = '5000000'
    mintTxOut = sender + '+' + lovelace + '+\"1 ' + policyID + '.' + tokenName + '\"'
    mint = ' --mint=\"1 6b4c0ea94391ce0b909e06ee7aa3350ccb1aa2032928466d2959c3f2.AuthNFT\"'

    # query protocol parameters
    command0 = "cardano-cli query protocol-parameters --testnet-magic 1097911063 --out-file protocol.json"
    
    # refactor so that everything is a variable and the string is just a list of them
    command1 = "cardano-cli transaction build --alonzo-era --tx-in " + txin + " --tx-out " + mintTxOut + " --change-address " + sender + mint + " --mint-redeemer-value 1 --mint-script-file " + scriptFile + " --tx-in-collateral " + collateral + " --testnet-magic 1097911063 --protocol-params-file protocol.json --out-file mint.raw"

    command2 = [
        'cardano-cli', 'transaction', 'sign',
        '--signing-key-file', senderkey,
        '--testnet-magic', '1097911063',
        '--tx-body-file', 'mint.raw',
        '--out-file', 'mint.signed'
    ]
    command3 = [
        'cardano-cli', 'transaction', 'submit',
        '--tx-file', 'mint.signed',
        '--testnet-magic', '1097911063'
    ]

    # print(mintTxOut)
    # print(mint)

    print(command1)

    path = os.path.join(tx_dir, 'temp')
    os.chdir(path)
    
    subprocess.run(command0, env=current, shell=True)
    print(' --- build ---')
    subprocess.run(command1, env=current, shell=True)
    print(' --- sign ---')
    subprocess.call(command2, env=current)
    print(' --- submit ---')
    subprocess.call(command3, env=current)
    print('So far so good')