#! python3
# ccliWallets.py

import subprocess, os
from pathlib import Path
import pyinputplus as pyin

import utxoUtils

def createWallet(env):
    print('')
    print('------------------------------------------------------------------------------------')
    print('Please enter a name for your new wallet.')
    wName = pyin.inputStr()
    wallet_dir = env["WALLET_DIR"] / wName
    os.makedirs(wallet_dir)
    command1 = [
        'cardano-cli', 'address', 'key-gen',
        '--verification-key-file', (wallet_dir / 'payment.vkey'),
        '--signing-key-file', (wallet_dir / 'payment.skey')
    ]
    command2 = [
        'cardano-cli', 'address', 'build',
        '--payment-verification-key-file', (wallet_dir / 'payment.vkey'),
        '--out-file', (wallet_dir / 'payment.addr'),
        '--testnet-magic', '1097911063'
    ]
    command3 = [
        'cardano-cli', 'stake-address', 'key-gen',
        '--verification-key-file', (wallet_dir / 'stake.vkey'),
        '--signing-key-file', (wallet_dir / 'stake.skey')
    ]
    command4 = [
        'cardano-cli', 'stake-address', 'build',
        '--stake-verification-key-file', (wallet_dir / 'stake.vkey'),
        '--testnet-magic', '1097911063',
        '--out-file', (wallet_dir / 'stake.addr')
    ]
    command5 = [
        'cardano-cli', 'address', 'build',
        '--payment-verification-key-file', (wallet_dir / 'payment.vkey'),
        '--stake-verification-key-file', (wallet_dir / 'stake.vkey'),
        '--testnet-magic', '1097911063',
        '--out-file', (wallet_dir / 'base.addr')
    ]
    subprocess.call(command1)
    subprocess.call(command2)
    subprocess.call(command3)
    subprocess.call(command4)
    subprocess.call(command5)
    return wName
    # see OCG's alternative to "return"


def viewWallet(env):
    selectWallet(env, True)
    utxoUtils.queryUTXOs(env)

def selectWallet(env, choice):
    allWallets = (env["WALLET_DIR"])
    w = {}
    l = 'a'
    dirlist = [ item for item in os.listdir(allWallets) if os.path.isdir(os.path.join(allWallets, item)) ]

    for d in dirlist:
        w[l] = d
        l = chr(ord(l) + 1)

    for k in w.keys():
        print('[' + k + '] > ' + w[k])

    if (choice):
    # currently fails if less than two wallets exist
        chooseWallet = pyin.inputChoice(list(w.keys()))
        walletName = w[chooseWallet]
        env["CURRENT_WALLET"] = walletName
        return walletName