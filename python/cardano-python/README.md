# cardano python

## Getting Started
- In `myWallet.py`,

## `plutusUtils.py`
### `createScriptAddress()`
- menu item (i)

### `queryScriptAddress()`
- menu item (j)

### `createDatumHash()`
- menu item (k)

### `lockAuthNFT()`
- menu item (l)

### `addFundsToContract()`
- menu item (n)

### `useAuthNFT()`
- menu item (m)
