#! python3
# plutusUtils.py
# Sources:
# Datums_redeemer_tutorial.md from Alonzo-Testnet repo

# Question for later - what is this 42 parameter doing? 
# in: cabal run plutus-helloworld -- 42 datum-redeemer.plutus

import subprocess, os
import pyinputplus as pyin

network = "--testnet-magic 1097911063"
# network = "--mainnet"

# --------------------------------------------------------------------------------------------------------------------------------------
# createScriptAddress()
# create a contract address from pre-compiled Plutus script
#
# --------------------------------------------------------------------------------------------------------------------------------------

def createScriptAddress(current):
    plutusScript = "path to plutus script"
    print("Script Address for Contract (compiled from mkValidator)")
    command0 = "cardano-cli address build --payment-script-file "+plutusScript+" "+network
    subprocess.run(command0, env=current, shell=True)
    print(" ")

# for now, just grab that script address and paste it here:

# --------------------------------------------------------------------------------------------------------------------------------------
# queryScriptAddress()
# query script address
#
# --------------------------------------------------------------------------------------------------------------------------------------

def queryScriptAddress(current):
    scriptAddress = "addr_test1wrptzmvexsfggduuc3pr6az0ncx869w9z4m93w505s4kr0cz0cde5"
    print("Utxos at Script Address " + scriptAddress)
    command0 = "cardano-cli query utxo --address "+scriptAddress+" "+network
    subprocess.run(command0, env=current, shell=True)
    print(" ")

# --------------------------------------------------------------------------------------------------------------------------------------
# createDatumHash()
# If you need some datum hashed - for example could be any integer, as in ValidateWithAuth.hs ie:
#
# --------------------------------------------------------------------------------------------------------------------------------------

def createDatumHash(current):
    print("What integer would you like to hash for datum?")
    datum = input()
    print("Datum hash for "+datum)
    command0 = "cardano-cli transaction hash-script-data --script-data-value "+datum
    subprocess.run(command0, env=current, shell=True)
    print(" ")

# --------------------------------------------------------------------------------------------------------------------------------------
# lockAuthNFT()
#
# Notes:
# need a txin utxo that includes an AuthNFT, and we need to include AuthNFT in a tx-out to scriptAddress
# Need redeemer - should match the 94 seen in ValidateWithAuth.hs
#
# !!! Locking and Unlocking require pretty different transactions
# --------------------------------------------------------------------------------------------------------------------------------------

def lockAuthNFT(wallet_dir, current):
    scriptAddress = "addr_test1wrptzmvexsfggduuc3pr6az0ncx869w9z4m93w505s4kr0cz0cde5"
    datumhash='52d800d39486d8234e08050de3fa06296497a3a44343b4494801eb502ce38f93' # datum hash for 1234
    sender = 'payment_address'
    senderkey = 'path_to_skey'
    txin = "get a utxo"
    txinAuth = "and the utxo with the AuthNFT"

    # build
    ccli='cardano-cli transaction build --alonzo-era'
    txinFund = "--tx-in "+txin
    txinWithAuthNFT = "--tx-in "+txinAuth
    txout = "--tx-out "+scriptAddress+"+15000000+\"1 6b4c0ea94391ce0b909e06ee7aa3350ccb1aa2032928466d2959c3f2.AuthNFT\""
    txoutDatumHash = "--tx-out-datum-hash "+datumhash
    change= "--change-address "+sender
    protocolParams='--protocol-params-file protocol.json'
    outFile='--out-file lock.raw'
    
    # sign
    ccliSign='cardano-cli transaction sign'
    signingKey="--signing-key-file "+senderkey
    txBodyFile="--tx-body-file lock.raw"
    outFileSigned="--out-file lock.signed"

    # submit
    ccliSubmit='cardano-cli transaction submit'
    txSignedFile="--tx-file lock.signed"
    
    build = " ".join([ccli, network, txinFund, txinWithAuthNFT, txout, txoutDatumHash, change, protocolParams, outFile])
    sign = " ".join([ccliSign, signingKey, network, txBodyFile, outFileSigned])
    submit = " ".join([ccliSubmit, txSignedFile, network])
    
    subprocess.run(build, env=current, shell=True)
    subprocess.run(sign, env=current, shell=True)
    subprocess.run(submit, env=current, shell=True)
    print("So far so good?")

# --------------------------------------------------------------------------------------------------------------------------------------
# addFundsToContract()
# Just add some utxos to a Contract Address, including Hashed Data
# 
# Notes
# Still need to figure out how to fund a contract address...
# ...and how to filter a [TxOut] in Plutus
# 
# --------------------------------------------------------------------------------------------------------------------------------------

def addFundsToContract(current):
    # build = " ".join([ccli, network, txinFundA, txinFundB, txinFundC, txoutFunder, txoutDatumHash, change, protocolParams, outFile])
    # sign = " ".join([ccliSign, signingKey, network, txBodyFile, outFileSigned])
    # submit = " ".join([ccliSubmit, txSignedFile, network])
    # subprocess.run(build, env=current, shell=True)
    # subprocess.run(sign, env=current, shell=True)
    # subprocess.run(submit, env=current, shell=True)
    print("Funds added?")

# Only required for "unlocking"


# --------------------------------------------------------------------------------------------------------------------------------------
# useAuthNFT
# This transaction will pass as long as the conditions in validate-with-auth.plutus are met
# 
# --------------------------------------------------------------------------------------------------------------------------------------

def useAuthNFT(wallet_dir, current):
    # input parameters
    plutusScript = "path to script"
    scriptAddress = "addr_test1wrptzmvexsfggduuc3pr6az0ncx869w9z4m93w505s4kr0cz0cde5"
    collateral = 'utxo'
    datumhash='52d800d39486d8234e08050de3fa06296497a3a44343b4494801eb502ce38f93' # datum hash for 1234

    utxoContract='utxo at contract address' # Held 13.5 ada
    utxoFundingFromWallet='utxo from wallet' # Held 14.833047 ada

    sender = 'address'
    senderkey = 'path to payment.skey'

    # build
    ccliBuild='cardano-cli transaction build --alonzo-era'
    txinFromContract = "--tx-in "+utxoContract
    txinFromWallet = "--tx-in "+utxoFundingFromWallet
    txinScriptFile = "--tx-in-script-file "+plutusScript
    txinDatumVal = '--tx-in-datum-value 11'
    txinRedeemerVal = '--tx-in-redeemer-value 94'
    txinCollateral = "--tx-in-collateral "+collateral
    txoutToScriptA = "--tx-out "+scriptAddress+"+13500000+\"1 6b4c0ea94391ce0b909e06ee7aa3350ccb1aa2032928466d2959c3f2.AuthNFT\""
    txoutDatumHash = "--tx-out-datum-hash "+datumhash
    change= "--change-address "+sender
    protocolParams='--protocol-params-file protocol.json'
    outFileUse = '--out-file use-auth-tx.raw'
    
    # sign
    ccliSign='cardano-cli transaction sign'
    signingKey="--signing-key-file "+senderkey
    txBodyFile="--tx-body-file use-auth-tx.raw"
    outFileSigned="--out-file use-auth-tx.signed"

    # submit
    ccliSubmit='cardano-cli transaction submit'
    txSignedFile="--tx-file use-auth-tx.signed"

    build = " ".join([
                        ccliBuild, 
                        txinFromContract, # must include Script, Datum, and Redeemer
                        txinScriptFile,
                        txinDatumVal, 
                        txinRedeemerVal, 
                        txinFromWallet, # Optional, to add funds to the script UTXO. Do not include Script, Datum, or Redeemer
                        txinCollateral, 
                        txoutToScriptA, 
                        txoutDatumHash, 
                        change, 
                        protocolParams, 
                        outFileUse, 
                        network
                    ])
    
    sign = " ".join([
                        ccliSign, 
                        signingKey, 
                        network, 
                        txBodyFile, 
                        outFileSigned
                    ])
    
    submit = " ".join([
                        ccliSubmit, 
                        txSignedFile, 
                        network
                    ])

    subprocess.run(build, env=current, shell=True)
    subprocess.run(sign, env=current, shell=True)
    subprocess.run(submit, env=current, shell=True)
    print("useAuthNFT() succesful?")

# The useAuthNFT works!
# So now: we need more utxos at that address with datum hash
# Then we can test some things, like whether this or that redeemer work, or with/wihtout tokens?
# Change goes back to wallet - but can it stay in utxo, hashed? Is this in Plutus?

# Yes - it's going to be a Plutus problem to further fund a contract UTXO -- and a Python optimization to set output Lovelace for max uses.


# Now, what is the cost of using this thing? Well, it appears that it's currently at alomst 0.4 Ada per "count". That's not going to do.
# In other words on 100 NFTs, we'd be spending 40 ada just to have the counter in place

# We'll also need to further fund the contract address so that it can do its work...

# Here is an example of the code we need
# cardano-cli transaction build \
# --alonzo-era \
# --tx-in 49ac2bf6a40df00d071b649d1e5b5d3531ddb2bfe6c619f9506cacb557bcb8b6#1 \
# --tx-in-script-file datum-redeemer.plutus \
# --tx-in-datum-value 42 \
# --tx-in-redeemer-value 42 \
# --tx-in-collateral cd4a34a97e8845631c57ec21fed3901e4a4f244e0673eb9e5d478437ec3e9bf4#1 \
# --change-address $(cat payment.addr) \
# --protocol-params-file protocol.json \
# --out-file tx.raw \
# --testnet-magic 8
# ----------------------------------------------------------------------------



# 4 #
# grepping utxo data!

# 5 #
# Calculate datum hash
# Take input to --script-data-value
# $ cardano-cli transaction hash-script-data --script-data-value 42

# 6 #
# Query personal UTxOs -- this should be a cardanoUtil, or its own file. Neat regex practice!!

# 7 #
# Query protocol parameters
# should be moved to cardanoUtils or split things up further

# 8 #
# Finally, transaction building. We can have some fun here.
# Take datum and redeemer as inputs
# Where can we pull datum hashes from?