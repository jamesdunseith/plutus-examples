#! python3
# pythonUtils.py

import subprocess, os
import pyinputplus as pyin

items = {
    'a': 'Query Tip',
    'b': 'List Available Wallets',
    'c': 'Query UTxOs for an Address',
    'd': 'Create Wallet',
    'e': 'Check ADA Amount in Wallet',
    'f': 'Build a simple transaction',
    'g': 'Make a set of collateral transactions (set parameters in ccliTransaction.py)',
    'h': 'Mint Auth NFT (configure in mintingUtils.py)',
    'i': 'View policyID',
    'j': 'Query script address',
    'k': 'Hash an integer as datum',
    'l': 'Lock funds at a script address',
    'm': 'Use the auth nft!',
    'n': 'Add funds to contract'
}

def homeMenu(i):
    for k in i.keys():
        print('[' + k + '] > ' + i[k])
    choice = pyin.inputChoice(list(i.keys()))
    return choice

def go():
    out = homeMenu(items)
    return out
