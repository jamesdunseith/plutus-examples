#! python3
# ccliTransactions.py

import subprocess, os
import pyinputplus as pyin


import walletUtils as wallet

sender = 'address'
senderkey = 'path to payment.skey'
receiver = 'address'

txin = 'utxo'
amount = '20000000' # or enough
send = sender + '+' + amount

# what directory is this all happening in? tmp?

def guidedTransaction(env):
    wallet.loadWallet(env)
    print('So now we can build a transaction with address ' + env["CURRENT_ADDRESS"])
    print('The name of the wallet is: ' + env["CURRENT_WALLET"])
    wallet.chooseUtxo(env)

def transaction(tx_dir, current):
    command1 = [
        'cardano-cli', 'transaction', 'build',
        '--alonzo-era',
        '--tx-in', txin,
        '--tx-out', send,
        '--change-address', sender,
        '--testnet-magic', '1097911063',
        '--out-file', 'tx.raw'
    ]
    command2 = [
        'cardano-cli', 'transaction', 'sign',
        '--signing-key-file', senderkey,
        '--testnet-magic', '1097911063',
        '--tx-body-file', 'tx.raw',
        '--out-file', 'tx.signed'
    ]
    command3 = [
        'cardano-cli', 'transaction', 'submit',
        '--tx-file', 'tx.signed',
        '--testnet-magic', '1097911063'
    ]
    path = os.path.join(tx_dir, 'temp')
    os.mkdir(path)
    os.chdir(path)

    print(' --- build ---')
    subprocess.call(command1, env=current)
    print(' --- sign ---')
    subprocess.call(command2, env=current)
    print(' --- submit ---')
    subprocess.call(command3, env=current)
    print('Transaction succesful!')

def makeCollateral(tx_dir, current):
    command1 = [
    'cardano-cli', 'transaction', 'build',
    '--alonzo-era',
    '--tx-in', txin,
    '--tx-out', send,
    '--tx-out', send,
    '--tx-out', send,
    '--tx-out', send,
    '--tx-out', send,
    '--tx-out', send,
    '--tx-out', send,
    '--tx-out', send,
    '--tx-out', send,
    '--tx-out', send,
    '--change-address', sender,
    '--testnet-magic', '1097911063',
    '--out-file', 'tx.raw'
    ]
    command2 = [
        'cardano-cli', 'transaction', 'sign',
        '--signing-key-file', senderkey,
        '--testnet-magic', '1097911063',
        '--tx-body-file', 'tx.raw',
        '--out-file', 'tx.signed'
    ]
    command3 = [
        'cardano-cli', 'transaction', 'submit',
        '--tx-file', 'tx.signed',
        '--testnet-magic', '1097911063'
    ]
    path = tx_dir / 'temp'
    if (not path.is_dir()):
        os.mkdir(path)
    os.chdir(path)

    print(' --- build ---')
    subprocess.call(command1, env=current)
    print(' --- sign ---')
    subprocess.call(command2, env=current)
    print(' --- submit ---')
    subprocess.call(command3, env=current)
    print('Transaction succesful!')
