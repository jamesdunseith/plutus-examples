#! python3
# myWallet.py

import subprocess, os, sys
import pyinputplus as pyin
from pathlib import Path

import ccliQueryTip as tip
import ccliWallets as cw
import ccliTransactions as tx
import mintingUtils as minting
import plutusUtils as plutus
import utxoUtils
import getAda
import homeMenu

# Configure before using
# Then test configuration with [b] Query Tip
my_env = os.environ.copy()
my_env["CARDANO_NODE_SOCKET_PATH"] = "/Users/thehub/cardano/testnet/db/socket" # path to node.socket
my_env["WALLET_DIR"] = Path.home() / 'code' / 'plutus' / 'wallets' / 'testnet'
my_env["TX_DIR"] = Path.home() / 'code' / 'plutus' / 'wallets' / 'transactions'


print('------------------------------------------------------------------------------------')
print('Cardano Utils')
print('')
print('Current Wallet Folder:')
print(my_env["WALLET_DIR"])
print('')
print('Current CARDANO_NODE_SOCKET_PATH:')
print(my_env["CARDANO_NODE_SOCKET_PATH"])
print('')
print('------------------------------------------------------------------------------------')

option = homeMenu.go()

print('------------------------------------------------------------------------------------')
if option == 'a':
    print('Query Tip')
    tip.queryTipTestnet(my_env)
elif option == 'b':
    print('List Available Wallets')
    cw.listWallets(my_env, False)
elif option == 'c':
    print('Select a Wallet:')
    cw.viewWallet(my_env)
elif option == 'd':
    print('Create Wallet')
    currentWallet = cw.createWallet(my_env)
    print('CURRENT WALLET: ' + currentWallet)
elif option == 'e':
    currentWallet = cw.selectWallet(my_env, True)
    print('In wallet ' + currentWallet + ':')
    getAda.getAdaAmount(my_env, currentWallet)
    print('DONE')
elif option == 'f':
    print('Build a Transaction')
    print('First, select a wallet:')
    print('')
    tx.guidedTransaction(my_env)
elif option == 'g':
    print('Make collateral transactions')
    tx.makeCollateral(my_env)
elif option == 'h':
    minting.nftMinter(my_env)
elif option == 'i':
    plutus.createScriptAddress(my_env)
elif option == 'j':
    plutus.queryScriptAddress(my_env)
elif option == 'k':
    plutus.createDatumHash(my_env)
elif option == 'l':
    plutus.lockAuthNFT(my_env)
elif option == 'm':
    plutus.useAuthNFT(my_env)
elif option == 'n':
    plutus.addFundsToContract(my_env)
print('------------------------------------------------------------------------------------')


# learn how to properly call one program from another, this is basic.

# Say which one we're talking about here

# 4: Encrypt all of it

sys.exit()
