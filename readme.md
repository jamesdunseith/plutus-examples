# Case Study: Incrementing Password Chaos NFTs

- Current Plutus dependency: 8c83c4abe211b4bbcaca3cdf1b2c0e38d0eb683f
- To do: revise this with most recent checkout as shown in Plutus Starter (or somewhere else?)

## Files
- `/at-5e40945`includes Plutus examples bulit on on https://github.com/input-output-hk/plutus at `git checkout 5e409452303b2bd9a5381b0f97f9a47edd69767d`, as with the [SpaceBudz Marketplace](https://github.com/Berry-Pool/spacebudz)
    - `/at-5e40945/auth-from-set` (3)
    - `/at-5e40945/incrementing-datum` (4)
    - `/at-5e40945/validate-with-auth` (2)
    - `/at-5e40945/sb-test` (1)
    - `/at-5e40945/mint-dummy-task` (TPBL Week 5, PPBL Week 1-2 or so?)
    -
- `/at-alonzo-exercise` includes Plutus examples built on https://github.com/input-output-hk/plutus at `git checkout 8c83c4abe211b4bbcaca3cdf1b2c0e38d0eb683f`
    - `/at-alonzo-exercise/mint-nft` was used to create `6b4c0ea94391ce0b909e06ee7aa3350ccb1aa2032928466d2959c3f2.AuthNFT`
    - `/at-alonzo-exercise/plutus-scripts` is the original file where I test compilation process
    - `/at-alonzo-exercise/validate-and-mint` *CURRENT GOAL*
    - `/at-alonzo-exercise/validate-with-auth` *NEXT STEP 2021-12-10*
    - `/at-alonzo-exercise/validator-test` simple validator test that locks tx and requires correct Datum and Hash
- `/python` gradually building re-usable Python scripts
- `/transactions` space to build transactions, may need to delete `/temp` for some Python functions to work (fix this)
- this `readme.md`
- `variables.md` for quick reference, much of this has been moved to `/python`

# Exercise 1: Minting
## How to Mint TestToken Token

### 0. Compile a plutus minting script

Take a look at `/at-alonzo-exercise/mint-nft`. The Minting Validator in `/policies/src/MintNFT.hs` allows us to mint one "AuthNFT" at a time. You can change the name of this token and the number required at minting time. Notice how "amount" is a parameter: this is the Redeemer in the validator we're building here. In the transaction we'll build next, the number of AuthNFT's we're minting will have to match the integer passed as `mint-redeemer-value`, and according to the Plutus contract, both will have to equal 1. Note that this contract does not provide any limits on how many times it can be used, so it's probably not correct to call it an "NFT".

When you're ready, try to compile. (Need docs for this part.) - A precompiled `mint-nft.plutus` is provided so you can jump right in.

### 1. Export minting policy id

```
cardano-cli transaction policyid \
--script-file mint-nft.plutus > mint-nft.id
```

### 2. Make a Collateral TX

You'll need a local cardano node synced up with Testnet to proceed.

Assumes some variables have been set in bash:

```
$SENDER=$(cat ...path-to/base.addr)
$SENDERKEY = path to `payment.skey` for this address
$TXIN = get the TxID and TxHash for a sufficient TXIN. (note that if you already have a UTxO with a little bit of ada and no additional assets, this can serve as your collateral.)
$AMOUNT
```


```
cardano-cli transaction build \
--alonzo-era \
--tx-in $TXIN \
--tx-out $SENDER+$AMOUNT \
--change-address $SENDER \
--testnet-magic 1097911063 \
--out-file make-collateral.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file make-collateral.raw \
--out-file make-collateral.signed

cardano-cli transaction submit \
--tx-file make-collateral.signed \
--testnet-magic 1097911063
```

### 3. Now Mint!
```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $TXIN \
--tx-out $SENDER+2000000+"1 $POLICYID.AuthNFT" \
--change-address $SENDER \
--mint="1 $POLICYID.AuthNFT" \
--mint-redeemer-value 1 \
--mint-script-file mint-nft.plutus \
--tx-in-collateral $COLLATERAL \
--protocol-params-file protocol.json \
--out-file check-amount.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file check-amount.raw \
--out-file check-amount.signed

cardano-cli transaction submit \
--tx-file check-amount.signed \
--testnet-magic 1097911063 \
```

## Simple Validator

### See `/validator-test`
- Use: https://github.com/input-output-hk/Alonzo-testnet/blob/main/Alonzo-tutorials/Datums_redeemer_tutorial.md

1. Compile a Validator contract to `validator-example.plutus`

2. Create a script address with cardano-cli

```
cardano-cli address build \
--payment-script-file validator-example.plutus \
$TESTNET \
--out-file validator-example.addr
```
Save that address for later
```
SCRIPTADDR=$(cat validator-example.addr)
```

2. Hash some datum. In this case, it's arbitrary, because our Validator should accept "any" datum.

```
cardano-cli transaction hash-script-data --script-data-value 555

DATUMHASH=0ccfc3774a415e77c64c293bc11f476f64a7ac4817c3debb8a3ace6b9e4d3595
```

3. Send & lock a tx at contract address

```
cardano-cli transaction build \
--alonzo-era \
--tx-in $TXIN \
--tx-out $SCRIPTADDR+4000000 \
--tx-out-datum-hash $DATUMHASH \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file tx.raw \
$TESTNET

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
$TESTNET \
--tx-body-file tx.raw \
--out-file tx.signed

cardano-cli transaction submit \
--tx-file tx.signed \
$TESTNET

```

So now we have a locked transaction. The datum CAN BE arbitrary but should not have been. Can you see why in `ValidatorExample.hs`?

4. To see what happens, try to unlock that tx

```
cardano-cli transaction build \
--alonzo-era \
$TESTNET \
--tx-in $TXIN \
--tx-in-script-file validator-example.plutus \
--tx-in-datum-value 555 \
--tx-in-redeemer-value 42 \
--tx-in-collateral $COLLATERAL \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file unlock.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
$TESTNET \
--tx-body-file unlock.raw \
--out-file unlock.signed

cardano-cli transaction submit \
--tx-file unlock.signed \
$TESTNET

```

We cannot unlock it! This tx fails because I've used one type of datum in the transaction while the Plutus expects another.

#### PBL Course Notes
- Task: build a transaction with an appropriate datum - you can change type expected in the Plutus script, or use the correct datum in the tx.
- Need to clean up the "broken" contracts first (including choosing the right version of Plutus), and provide the Validator + tx pair that don't work.
