# Building Background Knowledge

- [Official Plutus Repo](https://github.com/input-output-hk/plutus) <- docs are good for getting set up, but actual Plutus docs are limited
- [IOHK Plutus Documentation](https://playground.plutus.iohkdev.io/doc/haddock/)
- [Plutus Pioneers Code](https://github.com/input-output-hk/plutus-pioneer-program/tree/main/code)
- [What does $ do in Haskell?](https://typeclasses.com/featured/dollar)
- [SpaceBudz](https://github.com/Berry-Pool/spacebudz)
- [Quinn's Token Contract]()
- [Alonzo Testnet Exercises]()
- [Olga's Solutions to Alonzo Testnet 1-5]()
- [Cardano SDK](https://input-output-hk.github.io/cardano-js-sdk/index.html)