{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE BangPatterns               #-}

module Cardano.PlutusExample.AuthFromSet
  ( apiExampleAuthFromSet
  , authFromSetShortBs
  ) where

import qualified Prelude              as Haskell
--
import           Control.Monad        hiding (fmap)
import           Data.Aeson           (ToJSON, FromJSON,encode)
import           Data.List.NonEmpty   (NonEmpty (..))
import           Data.Text            (pack, Text)
import           GHC.Generics         (Generic)
import qualified PlutusTx
import           PlutusTx.Prelude     as P
import           Ledger               hiding (singleton)
import           Ledger.Credential    (Credential (..))
import           Ledger.Constraints   as Constraints
import qualified Ledger.Scripts       as Scripts
import qualified Ledger.Typed.Scripts as Scripts
import           Ledger.Value         as Value
import           Ledger.Ada           as Ada hiding (divide)
import           Prelude              ((/), Float, toInteger, floor)
import           Text.Printf          (printf)
import qualified PlutusTx.AssocMap    as AssocMap
import qualified Data.ByteString.Short as SBS
import qualified Data.ByteString.Lazy  as LBS
import           Cardano.Api hiding (Value, TxOut)
import           Cardano.Api.Shelley hiding (Value, TxOut)
import           Codec.Serialise hiding (encode)
import qualified Plutus.V1.Ledger.Api as Plutus

data MintingAuthTokenInfo = MintingAuthTokenInfo
  { authTokenSymbol :: !CurrencySymbol
  , authTokenName   :: !TokenName
  , seriesSize      :: !Integer
  }

PlutusTx.makeLift ''MintingAuthTokenInfo

passChaAuthTokenInfo = MintingAuthTokenInfo
  { authTokenSymbol = "6b4c0ea94391ce0b909e06ee7aa3350ccb1aa2032928466d2959c3f2"
  , authTokenName   = "AuthNFT"
  , seriesSize      = 94
  }

{-# INLINABLE mkValidator #-}
-- DATUM is held at script address in a utxo that can only be spent if certain conditions are met
-- REDEEMER is what happens if the transaction succeeds
-- we look at CONTEXT to see if AuthNFT is included in both incoming and outgoing utxos

mkValidator :: MintingAuthTokenInfo -> Integer -> Integer -> ScriptContext -> Bool
mkValidator passChaAuthTokenInfo@MintingAuthTokenInfo{..} _ r ctx = 
    traceIfFalse "redeemer should equal seriesSize" $ r == seriesSize && -- later, turn redeemer into a Type, ie "Deposit | Update "
    traceIfFalse "auth token missing from input" inputHasToken &&
    traceIfFalse "auth token missing from output" outputHasToken
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    -- I need to change this so that there's not just one TxOut, but a whole [TxOut], look at Spacebudz? And make a new copy of this contract.
    -- ownInput :: TxOut
    -- ownInput = case findOwnInput ctx of
    --     Nothing -> traceError "auth input missing"
    --     Just i -> txInInfoResolved i

    inputValue :: Value
    inputValue = valueSpent info

    inputHasToken :: Bool
    inputHasToken = valueOf inputValue authTokenSymbol authTokenName == 1
    -- inputHasToken = False

    ownOutput :: TxOut
    ownOutput = case getContinuingOutputs ctx of
        [o] -> o
        _ -> traceError "expected exactly one AuthNFT"

    outputHasToken :: Bool
    outputHasToken = valueOf (txOutValue ownOutput) authTokenSymbol authTokenName == 1
    -- outputHasToken = False

data MyTypes
instance Scripts.ValidatorTypes MyTypes where
  type instance DatumType MyTypes = Integer
  type instance RedeemerType MyTypes = Integer

typedValidator :: Scripts.TypedValidator MyTypes
typedValidator = Scripts.mkTypedValidator @MyTypes 
    ($$(PlutusTx.compile [|| mkValidator ||]) `PlutusTx.applyCode` PlutusTx.liftCode passChaAuthTokenInfo)
    $$(PlutusTx.compile [|| wrap ||])
  where
    wrap = Scripts.wrapValidator @Integer @Integer

validator :: Validator
validator = Scripts.validatorScript typedValidator

scriptAsCbor :: LBS.ByteString
scriptAsCbor = serialise validator

apiExampleAuthFromSet :: PlutusScript PlutusScriptV1
apiExampleAuthFromSet = PlutusScriptSerialised . SBS.toShort $ LBS.toStrict scriptAsCbor

authFromSetShortBs :: SBS.ShortByteString
authFromSetShortBs = SBS.toShort . LBS.toStrict $ scriptAsCbor