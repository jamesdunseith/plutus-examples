{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE BangPatterns               #-}

module Cardano.PlutusExample.MintDummyTask
  ( apiExampleMintDummyTask
  , mintDummyTaskShortBs
  ) where

import qualified Prelude              as Haskell
--
import           Control.Monad        hiding (fmap)
import           Data.Aeson           (ToJSON, FromJSON,encode)
import           Data.List.NonEmpty   (NonEmpty (..))
import           Data.Text            (pack, Text)
import           GHC.Generics         (Generic)
import qualified PlutusTx
import           PlutusTx.Prelude     as P
import           Ledger               hiding (singleton)
import           Ledger.Credential    (Credential (..))
import           Ledger.Constraints   as Constraints
import qualified Ledger.Scripts       as Scripts
import qualified Ledger.Typed.Scripts as Scripts
import           Ledger.Value         as Value
import           Ledger.Ada           as Ada hiding (divide)
import           Prelude              ((/), Float, toInteger, floor)
import           Text.Printf          (printf)
import qualified PlutusTx.AssocMap    as AssocMap
import qualified Data.ByteString.Short as SBS
import qualified Data.ByteString.Lazy  as LBS
import           Cardano.Api hiding (Value, TxOut)
import           Cardano.Api.Shelley hiding (Value, TxOut)
import           Codec.Serialise hiding (encode)
import qualified Plutus.V1.Ledger.Api as Plutus

data MintingAuthTokenInfo = MintingAuthTokenInfo
  { authTokenSymbol :: !CurrencySymbol
  , authTokenName   :: !TokenName
  }

PlutusTx.makeLift ''MintingAuthTokenInfo

tpblAuthTokenInfo = MintingAuthTokenInfo
  { authTokenSymbol = "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83"
  , authTokenName   = "tpblTestAuth"
  }

{-# INLINABLE mkPolicy #-}

mkPolicy :: Integer -> MintingAuthTokenInfo -> Integer -> ScriptContext -> Bool
mkPolicy amount tpblAuthTokenInfo@MintingAuthTokenInfo{..} _ ctx =
    traceIfFalse "auth token missing from input" inputHasToken &&
    traceIfFalse "auth token missing from output" outputHasToken &&
    traceIfFalse "must mint exactly one dummyTask" checkTokenName
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    -- I need to change this so that there's not just one TxOut, but a whole [TxOut], look at Spacebudz? And make a new copy of this contract.
    ownInput :: TxOut
    ownInput = case findOwnInput ctx of
        Nothing -> traceError "auth input missing"
        Just i -> txInInfoResolved i

    inputHasToken :: Bool
    inputHasToken = valueOf (txOutValue ownInput) authTokenSymbol authTokenName == 1
    -- inputHasToken = False

    ownOutput :: TxOut
    ownOutput = case getContinuingOutputs ctx of
        [o] -> o
        _ -> traceError "expected exactly one AuthNFT"

    outputHasToken :: Bool
    outputHasToken = valueOf (txOutValue ownOutput) authTokenSymbol authTokenName == 1
    -- outputHasToken = False

    checkTokenName :: Bool
    checkTokenName = case flattenValue (txInfoMint info) of
      [(cs, tn, amt)]   -> cs  == ownCurrencySymbol ctx && tokenNameIsValid tn && amt == amount
      _               -> False

    tokenNameIsValid :: TokenName -> Bool
    tokenNameIsValid tn = tn == TokenName "dummyTask"

data MyTypes
instance Scripts.ValidatorTypes MyTypes where
  type instance RedeemerType MyTypes = Integer
  -- not deleting instance types because we will use them soon

typedPolicy ::

typedValidator :: Scripts.TypedValidator MyTypes
typedValidator = Scripts.mkTypedValidator @MyTypes
    ($$(PlutusTx.compile [|| mkValidator ||]) `PlutusTx.applyCode` PlutusTx.liftCode tpblAuthTokenInfo)
    $$(PlutusTx.compile [|| wrap ||])
  where
    wrap = Scripts.wrapValidator @Integer @Integer

validator :: Validator
validator = Scripts.validatorScript typedValidator

scriptAsCbor :: LBS.ByteString
scriptAsCbor = serialise validator

apiExampleMintDummyTask :: PlutusScript PlutusScriptV1
apiExampleMintDummyTask = PlutusScriptSerialised . SBS.toShort $ LBS.toStrict scriptAsCbor

mintDummyTaskShortBs :: SBS.ShortByteString
mintDummyTaskShortBs = SBS.toShort . LBS.toStrict $ scriptAsCbor