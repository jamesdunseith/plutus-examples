{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE BangPatterns               #-}

module Cardano.PlutusExample.SbTest
  ( apiExampleSbTest
  , sbTestShortBs
  ) where

import qualified Prelude              as Haskell
--
import           Control.Monad        hiding (fmap)
import           Data.Aeson           (ToJSON, FromJSON,encode)
import           Data.List.NonEmpty   (NonEmpty (..))
import           Data.Text            (pack, Text)
import           GHC.Generics         (Generic)
import qualified PlutusTx
import           PlutusTx.Prelude     as P
import           Ledger               hiding (singleton)
import           Ledger.Credential    (Credential (..))
import           Ledger.Constraints   as Constraints
import qualified Ledger.Scripts       as Scripts
import qualified Ledger.Typed.Scripts as Scripts
import           Ledger.Value         as Value
import           Ledger.Ada           as Ada hiding (divide)
import           Prelude              ((/), Float, toInteger, floor)
import           Text.Printf          (printf)
import qualified PlutusTx.AssocMap    as AssocMap
import qualified Data.ByteString.Short as SBS
import qualified Data.ByteString.Lazy  as LBS
import           Cardano.Api hiding (Value, TxOut)
import           Cardano.Api.Shelley hiding (Value, TxOut)
import           Codec.Serialise hiding (encode)
import qualified Plutus.V1.Ledger.Api as Plutus


{-# INLINABLE mkValidator #-}
-- DATUM is held at script address in a utxo that can only be spent if certain conditions are met
-- REDEEMER is what happens if the transaction succeeds
-- we look at CONTEXT to see if AuthNFT is included in both incoming and outgoing utxos

mkValidator :: Integer -> Integer -> ScriptContext -> Bool
mkValidator _ r ctx = 
    traceIfFalse "redeemer should equal 20" $ r == 20 && -- later, turn redeemer into a Type, ie "Deposit | Update "
    traceIfFalse "auth token missing from input" inputHasToken &&
    traceIfFalse "auth token missing from output" outputHasToken
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    ownInput :: TxOut
    ownInput = case findOwnInput ctx of
        Nothing -> traceError "auth input missing"
        Just i -> txInInfoResolved i

    inputHasToken :: Bool
    -- inputHasToken = assetClassValueOf (txOutValue ownInput) authAsset == 1
    inputHasToken = False

    ownOutput :: TxOut
    ownOutput = case getContinuingOutputs ctx of
        [o] -> o
        _ -> traceError "expected exactly one AuthNFT"

    outputHasToken :: Bool
    -- outputHasToken = assetClassValueOf (txOutValue ownOutput) authAsset == 1
    outputHasToken = False

data Typed
instance Scripts.ValidatorTypes Typed where
  type instance DatumType Typed = Integer
  type instance RedeemerType Typed = Integer

typedValidator :: Scripts.TypedValidator Typed
typedValidator = Scripts.mkTypedValidator @Typed 
    $$(PlutusTx.compile [|| mkValidator ||])
    $$(PlutusTx.compile [|| wrap ||])
  where
    wrap = Scripts.wrapValidator @Integer @Integer

validator :: Validator
validator = Scripts.validatorScript typedValidator

scriptAsCbor :: LBS.ByteString
scriptAsCbor = serialise validator

apiExampleSbTest :: PlutusScript PlutusScriptV1
apiExampleSbTest = PlutusScriptSerialised . SBS.toShort $ LBS.toStrict scriptAsCbor

sbTestShortBs :: SBS.ShortByteString
sbTestShortBs = SBS.toShort . LBS.toStrict $ scriptAsCbor