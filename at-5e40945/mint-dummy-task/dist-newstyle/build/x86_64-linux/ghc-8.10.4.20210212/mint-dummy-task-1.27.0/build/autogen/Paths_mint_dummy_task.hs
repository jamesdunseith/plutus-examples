{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -Wno-missing-safe-haskell-mode #-}
module Paths_mint_dummy_task (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [1,27,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/root/.cabal/bin"
libdir     = "/root/.cabal/lib/x86_64-linux-ghc-8.10.4.20210212/mint-dummy-task-1.27.0-inplace"
dynlibdir  = "/root/.cabal/lib/x86_64-linux-ghc-8.10.4.20210212"
datadir    = "/root/.cabal/share/x86_64-linux-ghc-8.10.4.20210212/mint-dummy-task-1.27.0"
libexecdir = "/root/.cabal/libexec/x86_64-linux-ghc-8.10.4.20210212/mint-dummy-task-1.27.0"
sysconfdir = "/root/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "mint_dummy_task_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "mint_dummy_task_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "mint_dummy_task_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "mint_dummy_task_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "mint_dummy_task_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "mint_dummy_task_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
