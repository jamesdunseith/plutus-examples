{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Cardano.PlutusExample.ValidateWithAuth
  ( apiExampleValidateWithAuth
  , validateWithAuthShortBs
  ) where

import           Prelude                (IO, Semigroup (..), Show (..), String)

import           Cardano.Api.Shelley (PlutusScript (..), PlutusScriptV1)

import           Codec.Serialise
import           Data.ByteString
import           Data.ByteString.Char8
import qualified Data.ByteString.Lazy   as LB
import qualified Data.ByteString.Short  as SBS

import           Ledger hiding (singleton)
import qualified Ledger.Typed.Scripts   as Scripts
import           Ledger.Value           as Value
import qualified PlutusTx
import           PlutusTx                 (Data (..))
import           PlutusTx.Prelude hiding (Semigroup (..), unless)

-- helper definitions

-- data AuthParams = AuthParams
--   { authSymbol :: !CurrencySymbol
--   , authToken  :: !TokenName
--   } deriving (Show, Generic, Prelude.Eq, Prelude.Ord)

tS :: BuiltinByteString
tS = encodeUtf8 "6b4c0ea94391ce0b909e06ee7aa3350ccb1aa2032928466d2959c3f2"

tN :: ByteString
tN = Data.ByteString.Char8.pack "AuthNFT"

authSymbol :: CurrencySymbol
authSymbol = CurrencySymbol tS

authName :: TokenName
authName = tokenName tN

authAsset :: AssetClass
authAsset = assetClass authSymbol authName

-- authToken :: AssetClass
-- authToken = assetClass  "AuthNFT"

{-# INLINABLE mkValidator #-}
-- DATUM is held at script address in a utxo that can only be spent if certain conditions are met
-- REDEEMER is what happens if the transaction succeeds
-- we look at CONTEXT to see if AuthNFT is included in both incoming and outgoing utxos

mkValidator :: Integer -> Integer -> ScriptContext -> Bool
mkValidator _ r ctx = 
    traceIfFalse "redeemer should equal 20" $ r == 20 && -- later, turn redeemer into a Type, ie "Deposit | Update "
    traceIfFalse "auth token missing from input" inputHasToken &&
    traceIfFalse "auth token missing from output" outputHasToken
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    ownInput :: TxOut
    ownInput = case findOwnInput ctx of
        Nothing -> traceError "auth input missing"
        Just i -> txInInfoResolved i

    inputHasToken :: Bool
    inputHasToken = assetClassValueOf (txOutValue ownInput) authAsset == 1
    -- inputHasToken = False

    ownOutput :: TxOut
    ownOutput = case getContinuingOutputs ctx of
        [o] -> o
        _ -> traceError "expected exactly one AuthNFT"

    outputHasToken :: Bool
    outputHasToken = assetClassValueOf (txOutValue ownOutput) authAsset == 1
    -- outputHasToken = False

data Typed
instance Scripts.ValidatorTypes Typed where
  type instance DatumType Typed = Integer
  type instance RedeemerType Typed = Integer

typedValidator :: Scripts.TypedValidator Typed
typedValidator = Scripts.mkTypedValidator @Typed 
    $$(PlutusTx.compile [|| mkValidator ||])
    $$(PlutusTx.compile [|| wrap ||])
  where
    wrap = Scripts.wrapValidator @Integer @Integer

validator :: Validator
validator = Scripts.validatorScript typedValidator

scriptAsCbor :: LB.ByteString
scriptAsCbor = serialise validator

apiExampleValidateWithAuth :: PlutusScript PlutusScriptV1
apiExampleValidateWithAuth = PlutusScriptSerialised . SBS.toShort $ LB.toStrict scriptAsCbor

validateWithAuthShortBs :: SBS.ShortByteString
validateWithAuthShortBs = SBS.toShort . LB.toStrict $ scriptAsCbor
