{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Cardano.PlutusExample.ValidatorExample
  ( apiExampleValidatorExample
  , validatorExampleShortBs
  ) where

import           Prelude                (IO, Semigroup (..), Show (..), String)

import           Cardano.Api.Shelley (PlutusScript (..), PlutusScriptV1)

import           Codec.Serialise
import qualified Data.ByteString.Lazy   as LB
import qualified Data.ByteString.Short  as SBS

import           Ledger hiding (singleton)
import qualified Ledger.Typed.Scripts   as Scripts
import           Ledger.Value           as Value
import qualified PlutusTx
import           PlutusTx                 (Data (..))
import           PlutusTx.Prelude hiding (Semigroup (..), unless)


{-# INLINABLE mkValidator #-}
-- datum is
-- redeemer
-- context provides NFT
mkValidator :: Integer -> Integer -> ScriptContext -> Bool
mkValidator _ r _ = traceIfFalse "wrong redeemer" $ r == 42

-- Do these exercises, bit by bit
-- check that Redeemer is some string
-- check that Some Redeemer number matches some Number in Datum
-- make a special object type for redeemer so we can check number and name, or do some string magic with Haskell?

-- https://github.com/input-output-hk/Alonzo-testnet/blob/main/Alonzo-tutorials/Datums_redeemer_tutorial.md

data Typed
instance Scripts.ValidatorTypes Typed where
  type instance DatumType Typed = Integer
  type instance RedeemerType Typed = Integer

typedValidator :: Scripts.TypedValidator Typed
typedValidator = Scripts.mkTypedValidator @Typed 
    $$(PlutusTx.compile [|| mkValidator ||])
    $$(PlutusTx.compile [|| wrap ||])
  where
    wrap = Scripts.wrapValidator @Integer @Integer

validator :: Validator
validator = Scripts.validatorScript typedValidator

scriptAsCbor :: LB.ByteString
scriptAsCbor = serialise validator

apiExampleValidatorExample :: PlutusScript PlutusScriptV1
apiExampleValidatorExample = PlutusScriptSerialised . SBS.toShort $ LB.toStrict scriptAsCbor

validatorExampleShortBs :: SBS.ShortByteString
validatorExampleShortBs = SBS.toShort . LB.toStrict $ scriptAsCbor
